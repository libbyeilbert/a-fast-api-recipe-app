import React, { useState, useEffect } from 'react';
import { useRecipeContext } from '../recipes/RecipeContext';

function CocktailList() {
    const { fetchCocktails, cocktails } = useRecipeContext();

    useEffect(() => {
        fetchCocktails();
    }, []);

    return (
        <>
        <h1>Cocktails</h1>
        <ul>
            {cocktails.map(cocktail => {
                const url = `http://localhost:3000/cocktails/${cocktail.id}`
                return (
                    <li key={cocktail.id}>
                        <a href={url}>{cocktail.name}</a>
                    </li>
                )
            })}
        </ul>
        </>
    )
};
export default CocktailList;
