import React, { useState, useEffect } from 'react';
import styles from './margins.module.css';

function MarginCalculator() {

    const [margins, setMargins] = useState([]);

    const fetchMargins = async () => {
        const url = "http://localhost:8000/margins";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setMargins(data);
        };
    };

    useEffect(() => {
        fetchMargins();
    }, []);

    return (
        <>
        <table className={styles.marginstable}>
            <thead>
                <tr>
                    <th className={styles.marginstableheader}>Cocktail</th>
                    <th className={styles.marginstableheader}>Cost per Cocktail</th>
                    <th className={styles.marginstableheader}>Price</th>
                    <th className={styles.marginstableheader}>Margin</th>
                    <th className={styles.marginstableheader}>Suggested Retail -- 85% margin</th>
                </tr>
            </thead>
            <tbody>
                {margins.map(cocktail => {
                    const cocktailUrl = `http://localhost:3000/cocktails/${cocktail.id}`;
                    return (
                        <tr key={cocktail.id}>
                            <td className={styles.marginstableleftalign}>
                                <a href={cocktailUrl}>{cocktail.name}</a>
                                </td>
                            <td className={styles.marginstablerightalign}>${(cocktail.total).toFixed(2)}</td>
                            <td className={styles.marginstablerightalign}>${(cocktail.price).toFixed(2)}</td>
                            <td className={styles.marginstablerightalign}>{(cocktail.margin).toFixed(2)}%</td>
                            <td className={styles.marginstablerightalign}>${(cocktail.suggested_retail).toFixed(2)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
};
export default MarginCalculator;
