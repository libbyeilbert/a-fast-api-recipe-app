import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from '../recipes/RecipeContext';

function NewCocktail() {
    const { fetchCocktails } = useRecipeContext();
    const inputRef = useRef();
    const [name, setName] = useState('');
    const [baseCost, setBaseCost] = useState('');
    const [secondaryCost, setSecondaryCost] = useState('');
    const [tertiaryCost, setTertiaryCost] = useState('');
    const [mixerCost, setMixerCost] = useState('');
    const [price, setPrice] = useState('');
    const [drinksPerKeg, setDrinksPerKeg] = useState('');

    const handleNameChange = event => {
        setName(event.target.value);
    };

    const handleBaseCostChange = event => {
        setBaseCost(event.target.value);
    };

    const handleSecondaryCostChange = event => {
        setSecondaryCost(event.target.value);
    };

    const handleTertiaryCostChange = event => {
        setTertiaryCost(event.target.value);
    };

    const handleMixerCostChange = event => {
        setMixerCost(event.target.value);
    };

    const handlePriceChange = event => {
        setPrice(event.target.value);
    };

    const handleDrinksPerKegChange = event => {
        setDrinksPerKeg(event.target.value);
    };

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.base_cost = baseCost || 0;
        data.secondary_cost = secondaryCost || 0;
        data.tertiary_cost = tertiaryCost || 0;
        data.mixer_cost = mixerCost || 0;
        data.price = price || 0;
        data.drinks_per_keg = (drinksPerKeg * .97) || 1;

        const url = 'http://localhost:8000/cocktails';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchCocktails();
            setName('');
            setBaseCost('');
            setSecondaryCost('');
            setTertiaryCost('');
            setMixerCost('');
            setPrice('');
            setDrinksPerKeg('');
            inputRef.current.focus();
        };
    };

    useEffect(() => {
        inputRef.current.focus();
    }, []);
    return (
        <>
        <h1>New Cocktail</h1>
        <form onSubmit={handleSubmit} id="cocktail-form">
            <label htmlFor="name"></label>
            <div>
                <input ref={inputRef} value={name} onChange={handleNameChange} placeholder="Name" type="text" id="name" />
            </div>
            <label htmlFor="base-cost"></label>
            <div>
                <input value={baseCost} onChange={handleBaseCostChange} placeholder="Base Cost" type="text" id="base-cost" />
            </div>
            <label htmlFor="secondary-cost"></label>
            <div>
                <input value={secondaryCost} onChange={handleSecondaryCostChange} placeholder="Secondary Cost" type="text" id="secondary-cost" />
            </div>
            <label htmlFor="tertiary-cost"></label>
            <div>
                <input value={tertiaryCost} onChange={handleTertiaryCostChange} placeholder="Tertiary Cost" type="text" id="tertiary-cost" />
            </div>
            <label htmlFor="mixer-cost"></label>
            <div>
                <input value={mixerCost} onChange={handleMixerCostChange} placeholder="Mixer Cost" type="text" id="mixer-cost" />
            </div>
            <label htmlFor="price"></label>
            <div>
                <input value={price} onChange={handlePriceChange} placeholder="Price" type="text" id="price" />
            </div>
            <label htmlFor="drinks-per-keg"></label>
            <div>
                <input value={drinksPerKeg} onChange={handleDrinksPerKegChange} placeholder="Drinks Per Keg" type="text" id="drinks-per-keg" />
            </div>
            <button>Save</button>
        </form>
        </>
    );
 };
 export default NewCocktail;
