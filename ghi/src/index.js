import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadRecipes() {
  const response = await fetch('http://localhost:8000/recipes');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App recipes={data.recipes} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadRecipes();
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
