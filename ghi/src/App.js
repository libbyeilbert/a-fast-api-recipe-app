import { BrowserRouter, Routes, Route } from "react-router-dom";
import React from "react";
import { RecipeProvider } from './recipes/RecipeContext';
import Home from "./recipes/Home";
import Recipe from './recipes/Recipe';
import Nav from "./Nav";
import Cost from "./recipes/Cost";
import IngredientCost from "./recipes/IngredientCost";
import Calculate from "./recipes/Calculate";
import NewCocktail from "./margins/NewCocktail";
import CocktailList from "./margins/CocktailList";
import MarginCalculator from "./margins/MarginCalculator";
import EditCocktail from "./margins/EditCocktail";

function App() {

  return (
    <RecipeProvider>
    <BrowserRouter>
    <Nav />
      <div>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/:id" element={<Recipe />} />
          <Route path="cost/:id" element={<Cost />} />
          <Route path="ingredients/:recipeId/:id" element={<IngredientCost />} />
          <Route path="calculate/:id" element={<Calculate />} />
          <Route path="cocktails" element={<CocktailList />} />
          <Route path="cocktails/new" element={<NewCocktail />} />
          <Route path="cocktails/:id" element={<EditCocktail />} />
          <Route path="margins" element={<MarginCalculator />} />
        </Routes>
      </div>
    </BrowserRouter>
    </RecipeProvider>
  )
};

export default App;
