import { createContext, useContext, useState} from 'react';

const RecipeContext = createContext();

export function useRecipeContext() {
    return useContext(RecipeContext);
}

export function RecipeProvider({ children }) {
    const [recipes, setRecipes] = useState([]);
    const [cocktails, setCocktails] = useState([]);

    const updateRecipeName = (recipeId, newName) => {
        setRecipes(prevRecipes => {
            return prevRecipes.map(recipe => {
                if (recipe.id ===recipeId) {
                    return { ...recipe, name: newName };
                }
                return recipe
            });
        });
    };
    const fetchRecipes = async () => {
        const url = "http://localhost:8000/recipes/";
        const response = await fetch(url);
        if (response.ok) {
            const recipesData = await response.json();
            setRecipes(recipesData);
        }
    };
    const fetchCocktails = async () => {
        const url = "http://localhost:8000/cocktails";
        const response = await fetch(url);
        if (response.ok) {
            const cocktailsData = await response.json();
            setCocktails(cocktailsData);
        };
    };
    return (
        <RecipeContext.Provider value={{ recipes, cocktails, updateRecipeName, fetchRecipes, fetchCocktails }}>
            {children}
        </RecipeContext.Provider>
    );


}
