import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from './RecipeContext';

function Home() {
    const { fetchRecipes, recipes } = useRecipeContext();
    const inputRef = useRef();
    const [name, setName] = useState('');

    const handleNameChange = event => {
        setName(event.target.value);
    };




    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.name = name;

        const url = 'http://localhost:8000/recipes';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                setName('');
                await fetchRecipes();
            };
        } catch (error) {
            console.error('Error creating recipe:', error);
        }
    };

    useEffect(() => {
        inputRef.current.focus();
        fetchRecipes();
    }, []);


    return (
        <>
        <form onSubmit={handleSubmit} id="new-recipe-form">
            <label htmlFor="name"></label>
            <input ref={inputRef} value={name} onChange={handleNameChange} placeholder="Recipe Name" type="text" id="name" />
            <button>Create</button>
        </form>
        <ul>
            {recipes.map(recipe => {
                const recipeUrl = `http://localhost:3000/${recipe.id}`
                return (
                    <li key={recipe.id}>
                        <a href={recipeUrl}>{recipe.name}</a>
                    </li>
                )
            })}
        </ul>
        </>
    )
};

export default Home;
