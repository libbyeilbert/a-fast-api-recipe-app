import { useParams } from "react-router-dom";
import { useState, useEffect } from 'react';


function Cost() {
    const [recipe, setRecipe] = useState('');
    const [ingredients, setIngredients] = useState([]);
    const { id } = useParams();
    const [cost, setCost] = useState('');
    const [yieldAmount, setYieldAmount] = useState('');
    const [yieldUnit, setYieldUnit] = useState('');


    const fetchData = async () => {
        const url = `http://localhost:8000/recipes/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setRecipe(data.name);
            setYieldAmount(data.yield_amount);
            setYieldUnit(data.yield_unit);
        };
        const costUrl = `http://localhost:8000/costs/${id}`;
        const costResponse = await fetch(costUrl);
        if (costResponse.ok) {
            const costData = await costResponse.json();
            setIngredients(costData);
        }
        const totalUrl = `http://localhost:8000/recipes/${id}/cost`;
        const totalResponse = await fetch(totalUrl);
        if (totalResponse.ok) {
            const totalData = await totalResponse.json();
            setCost(totalData);
        };

    };

    const saveCost = async event => {
        event.preventDefault();
        const data = {}
        data.name = recipe;
        data.yield_amount = yieldAmount;
        data.yield_unit = yieldUnit;
        data.cost = cost;
        const url = `http://localhost:8000/recipes/${id}`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {

        }
    };


    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
        <h1>{recipe}</h1>
        <table>
            <thead>
                <tr>
                    <th>Ingredient</th>
                    <th>Amount per recipe</th>
                    <th>Ingredient cost</th>
                    <th>Total per recipe</th>
                </tr>
            </thead>
            <tbody>
                {ingredients.map(ingredient => {
                    const ingredientId = ingredient.id;
                    const ingredientUrl = `http://localhost:3000/ingredients/${id}/${ingredientId}`
                    return (
                        <tr key={ingredient.name}>
                            <td>{ingredient.name}</td>
                            <td>{ingredient.amount} {ingredient.unit}</td>
                            <td>${ingredient.cost_amount} per {ingredient.cost_unit}</td>
                            <td>${(ingredient.total || 0).toFixed(2)}</td>
                            <td>
                                <a href={ingredientUrl}>

                                Edit cost </a>

                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        <h3>Total cost:</h3>${(cost || 0).toFixed(2)}
        <button onClick={saveCost}>Save</button>
        <p>
                <a href={`http://localhost:3000/${id}`}>← Back to recipe</a>
</p>
        </>
    )
}
export default Cost;
