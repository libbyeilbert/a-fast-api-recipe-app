import { useParams, useNavigate } from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { useRecipeContext } from './RecipeContext';

function Calculate() {

    const { fetchRecipes } = useRecipeContext();
    const { id } = useParams();
    const [recipe, setRecipe] = useState('');
    const navigate = useNavigate();
    const [yield_amount, setYieldAmount] = useState('');
    const [yield_unit, setYieldUnit] = useState('');
    const [new_yield, setNewYield] = useState('');
    const [amounts, setAmounts] = useState({});
    const [ingredients, setIngredients] = useState([]);
    const [originalIngredients, setOriginalIngredients] = useState([]);
    const [batch_size, setBatchSize] = useState('1');
    const [costs, setCosts] = useState({});
    const [calculateClicked, setCalculateClicked] = useState(false);


    const fetchData = async () => {
        const url = `http://localhost:8000/recipes/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setRecipe(data.name);
            setNewYield(data.yield_amount);
            setYieldUnit(data.yield_unit);
            setYieldAmount(data.yield_amount);
        };
        const ingredientsUrl = `http://localhost:8000/costs/${id}`;
        const ingredientsResponse = await fetch(ingredientsUrl);
        if (ingredientsResponse.ok) {
            const ingredientsData = await ingredientsResponse.json();
            setIngredients(ingredientsData);
            setOriginalIngredients(ingredientsData);
            const initialAmounts = {};
            ingredientsData.forEach(ingredient => {
                initialAmounts[ingredient.id] = ingredient.amount;
            });
            const initialCosts = {};
            ingredientsData.forEach(ingredient => {
                initialCosts[ingredient.id] = ingredient.cost_amount;
            });
            setAmounts(initialAmounts);
            setCosts(initialCosts);
        };
    };


    const handleBatchSizeChange = event => {
        setBatchSize(event.target.value);
    };
    const handleYieldChange = event => {
        setNewYield(event.target.value);
    }
    const handleAmountChange = (event, ingredientId) => {
        const newAmount = parseFloat(event.target.value);
        setAmounts(prevAmounts => ({
            ...prevAmounts,
            [ingredientId]: newAmount,
        }));
        if(isNaN(newAmount)) {
            return;
        }
        const originalAmount = parseFloat(originalIngredients.find(item => item.id === ingredientId).amount);
        const multiplier = newAmount / originalAmount;
        setBatchSize(multiplier);
    };

    const handleYieldSubmit = async event => {
        event.preventDefault();
        const multiplier = parseFloat(new_yield) / parseFloat(yield_amount);
        const updatedAmounts = ingredients.reduce((newAmounts, ingredient) => {
            const originalAmount = parseFloat(ingredient.amount);
            newAmounts[ingredient.id] = originalAmount * multiplier;
            return newAmounts;
        }, {});
        setAmounts(updatedAmounts);
        setBatchSize(batch_size * multiplier);
        const updatedCosts = ingredients.reduce((newCosts, ingredient) => {
            const originalCost = parseFloat(ingredient.cost_amount);
            newCosts[ingredient.id] = originalCost * multiplier;
            return newCosts;
            }, {});
        setCosts(updatedCosts);
    }

const multiply = event => {
    event.preventDefault();
    const multiplier = parseFloat(batch_size);
    const updatedAmounts = ingredients.reduce((newAmounts, ingredient) => {
        const originalAmount = parseFloat(ingredient.amount);
        newAmounts[ingredient.id] = originalAmount * multiplier;
        return newAmounts;
        }, {});
    setAmounts(updatedAmounts);
    const updatedCosts = ingredients.reduce((newCosts, ingredient) => {
        const originalCost = parseFloat(ingredient.cost_amount);
        newCosts[ingredient.id] = (originalCost || 0) * multiplier;
        return newCosts;
        }, {});
    setCosts(updatedCosts);
    const updatedYield = parseFloat(yield_amount) * multiplier;
    setNewYield(updatedYield)
    setCalculateClicked(true);
};

const reset = event => {
    fetchData();
    setBatchSize('1');
}

const totalCost = ingredients.reduce((total, ingredient) => {
    const ingredientCost = ingredient.cost_amount * (amounts[ingredient.id] || 0);
    return total + ingredientCost;
}, 0);

const handleSaveVersion = async event => {
    const recipeData = {};
    recipeData.name = `${recipe} x${batch_size}`;
    recipeData.yield_amount = new_yield !== undefined ? new_yield : 0;
    recipeData.yield_unit = yield_unit !== undefined ? yield_unit : '';
    recipeData.cost = totalCost;
    const newIngredients = [];
    const recipeUrl = 'http://localhost:8000/recipes/'
    const recipeFetchConfig = {
        method: "post",
        body: JSON.stringify(recipeData),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const recipeResponse = await fetch(recipeUrl, recipeFetchConfig);
    if (recipeResponse.ok) {
        fetchRecipes();
        const newRecipe = await recipeResponse.json();
        const newRecipeId = newRecipe.id;
        originalIngredients.forEach(ingredient => {
            const updatedAmount = amounts[ingredient.id];
            const updatedCost = costs[ingredient.id] * (updatedAmount || 0);

            const newIngredient = {
                name: ingredient.name,
                amount: updatedAmount,
                unit: ingredient.unit,
                recipe: newRecipeId
            };
            newIngredients.push(newIngredient);
        });
        for (const newIngredient of newIngredients) {
            const ingredientResponse = await fetch(`http://localhost:8000/ingredients/`, {
                method: "post",
                body: JSON.stringify(newIngredient),
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            if (ingredientResponse.ok) {
                const newIngredientData = await ingredientResponse.json();
            }
        }
        navigate(`/${newRecipeId}`)
    }
};

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <h1>{recipe}</h1>
        <form>
        Multiply by: <input onChange={handleBatchSizeChange} value={batch_size} type="number" id="batch_size" />
        <button onClick={multiply}>Calculate</button>
        <a onClick={event => {
                        if (!calculateClicked) {
                            event.preventDefault();
                        } else {
                            handleSaveVersion();
                        }
                    }}>Save this version</a>
        </form>
        <table>
            <tbody>
                {ingredients.map(ingredient => {
                    return (
                        <tr key={ingredient.id}>
                            <td>
                                <input onChange={(event) => handleAmountChange(event, ingredient.id)} value={amounts[ingredient.id] || ''} type="number" id={`amount_${ingredient.id}`} />
                            </td>
                            <td>{ingredient.unit}</td>
                            <td>{ingredient.name}</td>
                            <td>${(costs[ingredient.id] * ingredient.amount).toFixed(2)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        <p>Total cost: ${totalCost.toFixed(2)}</p>
        <form onSubmit={handleYieldSubmit} id="yield-form">
        Yield: <input onChange={handleYieldChange} value={new_yield} type="number" id="new_yield" />
        {yield_unit}
        <button>Calculate</button>
        </form>
        <p>
            <button onClick={reset}>Reset recipe</button>
        </p>
        <a href={`http://localhost:3000/${id}`}>← Back to recipe</a>



        </>
    );
};
export default Calculate;
