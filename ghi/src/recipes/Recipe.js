import { useParams } from "react-router-dom";
import React, { useState, useEffect, useRef } from 'react';
import Popup from 'reactjs-popup';
import { useRecipeContext } from './RecipeContext';

function Recipe() {
    const { updateRecipeName, fetchRecipes, recipes } = useRecipeContext();
    const { id } = useParams();
    const amountInputRef = useRef();
    const [recipe, setRecipe] = useState('');
    const [ingredient, setIngredient] = useState('');
    const [amount, setAmount] = useState('');
    const [unit, setUnit] = useState('');
    const [recipeNames, setRecipeNames] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [yield_amount, setYieldAmount] = useState('');
    const [yield_unit, setYieldUnit] = useState('');
    const [recipe_add, setRecipeAdd] = useState('');
    const [editedAmount, setEditedAmount] = useState('');
    const [editedUnit, setEditedUnit] = useState('');
    const [editedIngredient, setEditedIngredient] = useState('');
    const [editedIngredientId, setEditedIngredientId] = useState('');
    const [cost, setCost] = useState('');

    const editAmount = event => {
        setEditedAmount(event.target.value);
    };

    const editUnit = event => {
        setEditedUnit(event.target.value);
    };

    const editName = event => {
        setEditedIngredient(event.target.value);
    };

    const handleIngredientChange = event => {
        setIngredient(event.target.value);
    };
    const handleAmountChange = event => {
        setAmount(event.target.value);
    };
    const handleUnitChange = event => {
        setUnit(event.target.value);
    };
    const handleRecipeAddChange = event => {
        setRecipeAdd(event.target.value);
    };

    const handleRecipeChange = event => {
        setRecipe(event.target.value);
    }
    const fetchIngredients  = async () => {
        const url = `http://localhost:8000/ingredients/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const ingredientsData = await response.json();
            setIngredients(ingredientsData);
        }
    }

    const handleDeleteRecipe = (recipeId) => {
    const url = `http://localhost:8000/recipes/${recipeId}`;
    fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application.json',
        },
    })
    .then(response => {
        if (response.ok) {
            window.location.href = 'http://localhost:3000/';
        }
    })

}


    const handleAddIngredient = async event => {
        event.preventDefault();
        const data = {}
        data.name = ingredient;
        data.amount = amount;
        data.unit = unit;
        data.recipe = id;
        if (!recipeNames.includes(ingredient)) {
        const url = `http://localhost:8000/api/ingredients/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchData();

        }};

        const versionUrl = 'http://localhost:8000/ingredients';
        const versionFetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const versionResponse = await fetch(versionUrl, versionFetchConfig);
        if (versionResponse.ok) {
            fetchData();
            setAmount('');
            setUnit('');
            setIngredient('');
            fetchIngredients();
            amountInputRef.current.focus();

        };
    };
    const handleYieldAmountChange = event => {
        setYieldAmount(event.target.value);
    };
    const handleYieldUnitChange = event => {
        setYieldUnit(event.target.value);
    };

    const handleYieldSubmit = async event => {
        event.preventDefault();
        const yieldData = {};
        yieldData.name = recipe;
        yieldData.yield_amount = parseFloat(yield_amount);
        yieldData.yield_unit = yield_unit;
        yieldData.cost = cost;
        const url = `http://localhost:8000/recipes/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(yieldData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setYieldAmount(yield_amount);
            setYieldUnit(yield_unit);
        }

    };

    const saveIngredient = async ingredientId => {

        const data = {};
        data.amount = parseFloat(editedAmount);
        data.unit = editedUnit;
        data.name = editedIngredient;
        data.recipe = id;
        const url = `http://localhost:8000/ingredients/${ingredientId}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {}

    }

    const handleAddRecipe = async event => {
        event.preventDefault();
        const selectedRecipe = recipes.find(recipe => recipe.name === recipe_add);

        if (!selectedRecipe) {
            console.log("Selected recipe not found");
            return;
    }
        const data = {};
        data.name = recipe_add;
        data.amount = 1;
        data.unit = 'recipe';
        data.recipe = id;
        data.cost_amount = selectedRecipe.cost;
        data.cost_unit = 'recipe';
        console.log(data);
 if (!recipeNames.includes(recipe_add)) {
        const url = `http://localhost:8000/api/ingredients/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchData();

        }};
        const versionUrl = `http://localhost:8000/ingredients`;
        const versionFetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const versionResponse = await fetch(versionUrl, versionFetchConfig);
        if (versionResponse.ok) {
            fetchData();
            fetchIngredients();
        }
    }

    const saveRecipeName = async event => {
        event.preventDefault();
        const data = {};
        data.name = recipe;

        const url = `http://localhost:8000/recipes/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setRecipe(recipe);
            updateRecipeName(recipe.id, recipe);
            fetchRecipes();
        }
    }

    const editIngredient = selectedIngredient => {
        setEditedIngredientId(selectedIngredient.id);
        setEditedIngredient(selectedIngredient.name);
        setEditedAmount(selectedIngredient.amount);
        setEditedUnit(selectedIngredient.unit);
    };

    const handleDelete = (ingredientId) => {
        const url = `http://localhost:8000/ingredients/${ingredientId}`;
        fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application.json',
            },
        })
        .then(response => {
            if (response.ok) {
                window.location.reload();
            }
        })

    }
    const fetchData = async () => {
        const recipeUrl = `http://localhost:8000/recipes/${id}`;
        const response = await fetch(recipeUrl);
        if (response.ok) {
            const data = await response.json();
            setRecipe(data.name);
            setYieldAmount(data.yield_amount);
            setYieldUnit(data.yield_unit);
            setCost(data.cost)
        };
    };

    const fetchRecipeNames = async () => {
        const url = 'http://localhost:8000/api/ingredients';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const list = [];
            {data.map(ingredient => {
                list.push(ingredient.name);
                setRecipeNames(list);
            });
        };
    }};

    useEffect(() => {
        fetchData();
        fetchIngredients();
        fetchRecipeNames();
        fetchRecipes();
        amountInputRef.current.focus();
    }, []);


    return (
        <>
            <h1>{recipe}</h1>
            <Popup trigger={
        <button>Edit recipe name</button>
        } position="right center">
    <div>
        <form onSubmit={saveRecipeName} id="recipe-name-change-form">
    <input value={recipe} onChange={handleRecipeChange} placeholder="recipe" type="text" id="recipe" />
    <button >Save</button> </form>

    </div>
  </Popup>
  <button onClick={() => handleDeleteRecipe(id)}>Delete recipe</button>

            <h2>Add an ingredient:</h2>
            <form onSubmit={handleAddIngredient} id="add-ingredient">
                <div>
                    <label htmlFor="amount"></label>
                    <input ref={amountInputRef} value={amount} onChange={handleAmountChange} placeholder="amount" type="text" id="amount" />
                    <label htmlFor="unit"></label>
                    <input value={unit} onChange={handleUnitChange} placeholder="unit" type="text" id ="unit"/>
                    <label htmlFor="ingredient"></label>
                    <input value={ingredient} onChange={handleIngredientChange} placeholder="ingredient" type="text" id="ingredient" />
                    <button>Add</button>
                </div>
            </form>
            <table>
        <tbody>
    {ingredients.map(ingredient => {
        const isEditing = editedIngredientId === ingredient.id;

        return (
            <React.Fragment key={ingredient.id}>
                <tr>
                    <td>{ingredient.amount}</td>
                    <td>{ingredient.unit}</td>
                    <td>{ingredient.name}</td>
                    <td>
                        <button

                            onClick={() => {
                                editIngredient(ingredient);
                            }}
                        >
                            Edit ingredient
                        </button>
                    </td>
                    <td>
                        <button

                            onClick={() => handleDelete(ingredient.id)}
                        >
                            Delete ingredient
                        </button>
                    </td>

                </tr>
                {isEditing && (
                    <tr>
                        <td colSpan="6">
                            <form>
                                <input
                                    value={editedAmount}
                                    onChange={editAmount}
                                    placeholder="amount"
                                    type="text"
                                    id="editAmount"
                                />
                                <input

                                    value={editedUnit}
                                    onChange={editUnit}
                                    placeholder="unit"
                                    type="text"
                                    id="editUnit"
                                />
                                <input
                                    value={editedIngredient}
                                    onChange={editName}
                                    placeholder="ingredient"
                                    type="text"
                                    id="editName"
                                />
                                <button onClick={() => saveIngredient(ingredient.id)}>Save</button>
                                <button onClick={() => {
                                    setEditedAmount('');
                                    setEditedUnit('');
                                    setEditedIngredient('');
                                }}>Cancel</button>
                            </form>
                        </td>
                    </tr>
                )}
            </React.Fragment>
        );
    })}
</tbody>
</table>
<h3>Add a recipe:</h3>
        <form onSubmit={handleAddRecipe} id="add-recipe-form">
       <select onChange={handleRecipeAddChange} id="recipeAdd">
        <option value="">Choose a recipe</option>
        {recipes.map(recipe => {
            return (
                <option key={recipe.id} value={recipe.name}>
                    {recipe.name}
                </option>            );
        })}
        </select>
        <button>Add</button>
        </form>


    <h2>Yield:</h2>
    <form onSubmit={handleYieldSubmit} id="yield-form">
        <label htmlFor="yield-amount"></label>
        <input value={yield_amount} onChange={handleYieldAmountChange} placeholder="amount" type="text" id="yield-amount" />
             <label htmlFor="yield-unit"></label>
   <input value={yield_unit} onChange={handleYieldUnitChange} placeholder="unit" type="text" id="yield-unit" />
   <button>Save</button>
    </form>
    <p>
    <a href={`http://localhost:3000/cost/${id}`}>Costs →</a>
    </p>
    <p>
    <a href={`http://localhost:3000/calculate/${id}`}>Batch calculations →</a>
    </p>
        </>
    );
};

export default Recipe;
