import { useParams } from "react-router-dom";
import React, { useState, useEffect, useRef } from 'react';

function IngredientCost() {
    const [ingredient, setIngredient] = useState('');
    const dollarInputRef = useRef();
    const { recipeId, id } = useParams();
    const [dollarInput, setDollarInput] = useState('');
    const [amountInput, setAmountInput] = useState('');
    const [unitInput, setUnitInput] = useState('');
    const [costAmount, setCostAmount] = useState('');
    const [costUnit, setCostUnit] = useState('');
    const [source, setSource] = useState('');

    const fetchData = async () => {
        const url = `http://localhost:8000/api/ingredients/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setIngredient(data.name);
            setCostAmount(data.cost_amount);
            setCostUnit(data.cost_unit);
            setSource(data.source);
        }
    }
    const handleSourceChange = event => {
        setSource(event.target.value);
    };
    const handleDollarInput = event => {
        setDollarInput(event.target.value);
    };
    const handleAmountInput = event => {
        setAmountInput(event.target.value);
    };
    const handleUnitInput = event => {
        setUnitInput(event.target.value);
    };

    const handleSubmit = async event => {
        event.preventDefault();
        const calculatedAmount = dollarInput / amountInput;
        const data = {};
        data.name = ingredient;
        data.cost_amount = calculatedAmount;
        data.cost_unit = unitInput;
        data.source = source;

        const url = `http://localhost:8000/api/ingredients/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setCostAmount(calculatedAmount);
            setCostUnit(unitInput);
        }
    };
    useEffect(() => {
        fetchData();
        dollarInputRef.current.focus();
    }, []);
    return (
        <>
        <h2>Cost of {ingredient}:</h2>
        <form onSubmit={handleSubmit} id="cost-form">
        <input ref={dollarInputRef} value={dollarInput} onChange={handleDollarInput} placeholder="$" type="number" id="dollars" /> per
        <input value={amountInput} onChange={handleAmountInput} placeholder="amount" type="number" id="amount" />
        <input value={unitInput} onChange={handleUnitInput} placeholder="unit" type="text" id="unit" />
        <p>Source:
            <input value={source} onChange={handleSourceChange} placeholder="" type="text" id="source" />
        </p>
        <button>Save</button>
        </form>
        <h3>${costAmount} per {costUnit}</h3>
                <a href={`http://localhost:3000/cost/${recipeId}`}>← Back to recipe costs</a>

        </>
    )
};
export default IngredientCost;
