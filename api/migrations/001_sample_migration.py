steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE recipes (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            yield_amount NUMERIC(20, 6),
            yield_unit VARCHAR(50),
            cost NUMERIC(10, 2)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE recipes;
        """,
    ],
    [
        """
        CREATE TABLE ingredients (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            cost_amount NUMERIC(10, 2),
            cost_unit VARCHAR(50),
            source VARCHAR(100)
        );
        """,
        """
        DROP TABLE ingredients;
        """,
    ],
    [
        """
        CREATE TABLE ingredient_version (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            amount NUMERIC(20, 6),
            unit VARCHAR(50),
            recipe INTEGER REFERENCES recipes("id") ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE ingredient_version;
        """,
    ],
    [
        """
        CREATE TABLE inventory (
            id SERIAL PRIMARY KEY NOT NULL,
            ingredient INTEGER UNIQUE REFERENCES ingredients("id") ON DELETE CASCADE,
            par_amount NUMERIC(10, 6),
            par_unit VARCHAR(50)
        );
        """,
        """
        DROP TABLE inventory;
        """,
    ],
    [
        """
        CREATE TABLE cocktail (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100),
            base_cost NUMERIC(10, 2),
            secondary_cost NUMERIC(10, 2),
            tertiary_cost NUMERIC(10, 2),
            mixer_cost NUMERIC(10, 2),
            price NUMERIC(10, 2),
            drinks_per_keg NUMERIC(10,2)
        );
        """,
        """
        DROP TABLE cocktail;
        """,
    ],
]
