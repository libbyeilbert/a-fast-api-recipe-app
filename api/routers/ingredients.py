from fastapi import APIRouter, Depends
from typing import List
from queries.ingredients import (
    IngredientOut,
    IngredientIn,
    IngredientQueries,
    IngredientVersionIn,
    IngredientVersionOut,
    IngredientCostOut,
)

router = APIRouter()


@router.get("/api/ingredients", response_model=List[IngredientOut])
def get_ingredients(queries: IngredientQueries = Depends()):
    return queries.get_ingredients()


@router.get("/api/ingredients/{ingredient_id}", response_model=IngredientOut)
def get_ingredient(ingredient_id: int, queries: IngredientQueries = Depends()):
    return queries.get_ingredient(ingredient_id)


@router.post("/api/ingredients", response_model=IngredientOut)
def create_ingredient(
    ingredient: IngredientIn,
    queries: IngredientQueries = Depends(),
):
    return queries.create_ingredient(ingredient)


@router.put("/api/ingredients/{ingredient_id}", response_model=IngredientOut)
def edit_ingredient(
    ingredient_id: int,
    ingredient: IngredientIn,
    queries: IngredientQueries = Depends(),
):
    return queries.edit_ingredient(ingredient_id, ingredient)


@router.delete("/api/ingredients/{ingredient_id}", response_model=bool)
def delete_ingredient(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    queries.delete(ingredient_id)
    return True


@router.get(
    "/ingredients/{recipe_id}", response_model=List[IngredientVersionOut]
)
def get_ingredient_versions(
    recipe_id: int, queries: IngredientQueries = Depends()
):
    return queries.get_ingredient_versions(recipe_id)


@router.get(
    "/ingredients/{ingredient_id}", response_model=IngredientVersionOut
)
def get_ingredient_version(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    return queries.get_ingredient_version(ingredient_id)


@router.post("/ingredients", response_model=IngredientVersionOut)
def create_ingredient_version(
    ingredient: IngredientVersionIn,
    queries: IngredientQueries = Depends(),
):
    return queries.create_ingredient_version(ingredient)


@router.put(
    "/ingredients/{ingredient_id}", response_model=IngredientVersionOut
)
def edit_ingredient_version(
    ingredient_id: int,
    ingredient: IngredientVersionIn,
    queries: IngredientQueries = Depends(),
):
    return queries.edit_ingredient_version(ingredient_id, ingredient)


@router.delete("/ingredients/{ingredient_id}", response_model=bool)
def delete_ingredient_version(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    queries.delete_version(ingredient_id)
    return True


@router.get("/costs/{recipe_id}", response_model=List[IngredientCostOut])
def get_costs(recipe_id: int, queries: IngredientQueries = Depends()):
    return queries.ingredient_cost_table(recipe_id)
