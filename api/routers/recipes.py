from fastapi import APIRouter, Depends
from typing import List
from queries.recipes import (
    RecipeOut,
    RecipeIn,
    RecipeQueries,
)

router = APIRouter()


@router.get("/recipes", response_model=List[RecipeOut])
def get_recipes(queries: RecipeQueries = Depends()):
    return queries.get_recipes()


@router.get("/recipes/{recipe_id}", response_model=RecipeOut)
def get_recipe(recipe_id: int, queries: RecipeQueries = Depends()):
    return queries.get_recipe(recipe_id)


@router.post("/recipes", response_model=RecipeOut)
def create_recipe(
    recipe: RecipeIn,
    queries: RecipeQueries = Depends(),
):
    return queries.create_recipe(recipe)


@router.put("/recipes/{recipe_id}", response_model=RecipeOut)
def edit_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    queries: RecipeQueries = Depends(),
):
    return queries.edit_recipe(recipe_id, recipe)


@router.delete("/recipes/{recipe_id}", response_model=bool)
def delete_recipe(recipe_id: int, queries: RecipeQueries = Depends()):
    queries.delete_recipe(recipe_id)
    return True


@router.get("/recipes/{recipe_id}/cost", response_model=float)
def get_recipe_cost(recipe_id: int, queries: RecipeQueries = Depends()):
    return queries.get_recipe_cost(recipe_id)
