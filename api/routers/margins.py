from fastapi import APIRouter, Depends
from typing import List
from queries.margins import (
    CocktailIn,
    CocktailOut,
    CocktailQueries,
    CocktailMargin,
)

router = APIRouter()


@router.get("/cocktails", response_model=List[CocktailOut])
def get_cocktails(queries: CocktailQueries = Depends()):
    return queries.get_cocktails()


@router.get("/margins", response_model=List[CocktailMargin])
def get_margins(queries: CocktailQueries = Depends()):
    return queries.get_margins()


@router.get("/cocktails/{cocktail_id}", response_model=CocktailOut)
def get_cocktail(cocktail_id: int, queries: CocktailQueries = Depends()):
    return queries.get_cocktail(cocktail_id)


@router.post("/cocktails", response_model=CocktailOut)
def create_cocktail(
    cocktail: CocktailIn, queries: CocktailQueries = Depends()
):
    return queries.create_cocktail(cocktail)


@router.put("/cocktails/{cocktail_id}", response_model=CocktailOut)
def edit_cocktail(
    cocktail_id: int,
    cocktail: CocktailIn,
    queries: CocktailQueries = Depends(),
):
    return queries.edit_cocktail(cocktail_id, cocktail)


@router.delete("/cocktails/{cocktail_id}", response_model=bool)
def delete_cocktail(cocktail_id: int, queries: CocktailQueries = Depends()):
    queries.delete(cocktail_id)
    return True
