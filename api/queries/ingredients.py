from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import Optional, List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class IngredientIn(BaseModel):
    name: str
    cost_amount: Optional[float]
    cost_unit: Optional[str]
    source: Optional[str]


class IngredientVersionIn(BaseModel):
    name: str
    amount: Optional[float]
    unit: Optional[str]
    recipe: Optional[int]


class IngredientOut(BaseModel):
    id: int
    name: str
    cost_amount: Optional[float]
    cost_unit: Optional[str]
    source: Optional[str]


class IngredientVersionOut(BaseModel):
    id: int
    name: str
    amount: Optional[float]
    unit: Optional[str]
    recipe: Optional[int]


class IngredientCostOut(BaseModel):
    name: str
    amount: Optional[float]
    unit: Optional[str]
    cost_amount: Optional[float]
    cost_unit: Optional[str]
    total: Optional[float]
    id: Optional[int]


class ParOut(BaseModel):
    id: int
    ingredient_id: int
    par_amount: float
    par_unit: str


class IngredientQueries:
    def get_ingredients(self) -> List[IngredientOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, cost_amount, cost_unit, source
                    FROM ingredients
                    """,
                )
                result = db.fetchall()
                return [
                    self.record_to_ingredient_out(record) for record in result
                ]

    def get_ingredient(self, ingredient_id: int) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, cost_amount, cost_unit, source
                    FROM ingredients
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                result = db.fetchone()
                return self.record_to_ingredient_out(result)

    def create_ingredient(
        self, ingredient: IngredientIn
    ) -> IngredientOut | None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO ingredients (
                        name, cost_amount, cost_unit, source
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, name, cost_amount, cost_unit, source
                    """,
                    [
                        ingredient.name,
                        ingredient.cost_amount,
                        ingredient.cost_unit,
                        ingredient.source,
                    ],
                )

                id = result.fetchone()[0]
                return self.ingredient_in_to_out(id, ingredient)

    def edit_ingredient(
        self, id: int, ingredient: IngredientIn
    ) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE ingredients
                    SET name = %s,
                    cost_amount = %s,
                    cost_unit = %s,
                    source = %s
                    WHERE id = %s
                    """,
                    [
                        ingredient.name,
                        ingredient.cost_amount,
                        ingredient.cost_unit,
                        ingredient.source,
                        id,
                    ],
                )
                return self.ingredient_in_to_out(id, ingredient)

    def record_to_ingredient_out(self, record):
        return IngredientOut(
            id=record[0],
            name=record[1],
            cost_amount=record[2],
            cost_unit=record[3],
            source=record[4],
        )

    def ingredient_in_to_out(self, id: int, ingredient: IngredientIn):
        old_data = ingredient.dict()
        return IngredientOut(id=id, **old_data)

    def delete(self, ingredient_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM ingredients
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                return True

    def get_ingredient_versions(
        self, recipe_id: int
    ) -> List[IngredientVersionOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, amount, unit, recipe
                    FROM ingredient_version
                    WHERE recipe = %s
                    """,
                    [recipe_id],
                )
                result = db.fetchall()
                return [
                    self.record_to_version_out(record) for record in result
                ]

    def get_ingredient_version(self, version_id: int) -> IngredientVersionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, amount, unit, recipe
                    FROM ingredient_version
                    WHERE id = %s
                    """,
                    [version_id],
                )
                result = db.fetchone()
                return self.record_to_version_out(result)

    def create_ingredient_version(
        self, ingredient: IngredientVersionIn
    ) -> IngredientVersionOut | None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO ingredient_version (
                        name, amount, unit, recipe
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, name, amount, unit, recipe
                    """,
                    [
                        ingredient.name,
                        ingredient.amount,
                        ingredient.unit,
                        ingredient.recipe,
                    ],
                )

                id = result.fetchone()[0]
                return self.version_in_to_out(id, ingredient)

    def edit_ingredient_version(
        self, id: int, ingredient: IngredientVersionIn
    ) -> IngredientVersionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE ingredient_version
                    SET name = %s,
                    amount = %s,
                    unit = %s,
                    recipe = %s
                    WHERE id = %s
                    """,
                    [
                        ingredient.name,
                        ingredient.amount,
                        ingredient.unit,
                        ingredient.recipe,
                        id,
                    ],
                )
                return self.version_in_to_out(id, ingredient)

    def record_to_version_out(self, record):
        return IngredientVersionOut(
            id=record[0],
            name=record[1],
            amount=record[2],
            unit=record[3],
            recipe=record[4],
        )

    def version_in_to_out(self, id: int, ingredient: IngredientVersionIn):
        old_data = ingredient.dict()
        return IngredientVersionOut(id=id, **old_data)

    def delete_version(self, ingredient_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM ingredient_version
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                return True

    def record_to_cost_out(self, record):
        return IngredientCostOut(
            name=record[0],
            amount=record[1],
            unit=record[2],
            cost_amount=record[3],
            cost_unit=record[4],
            total=record[5],
            id=record[6],
        )

    def ingredient_cost_table(self, recipe_id: int) -> List[IngredientCostOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT iv.name,
                        iv.amount,
                        iv.unit,
                        i.cost_amount,
                        i.cost_unit,
                        (i.cost_amount * iv.amount) as total,
                        i.id
                    FROM ingredient_version as iv
                    INNER JOIN ingredients as i
                    ON iv.name = i.name
                    WHERE iv.recipe = %s
                    """,
                    [recipe_id],
                )
                result = db.fetchall()
                return [self.record_to_cost_out(record) for record in result]

    def create_par(self, ingredient_id: int) -> ParOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO inventory (

                    )
                    """
                )
