from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import Optional, List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class RecipeIn(BaseModel):
    name: str
    yield_amount: Optional[float]
    yield_unit: Optional[str]
    cost: Optional[float]


class RecipeOut(BaseModel):
    id: int
    name: str
    yield_amount: Optional[float]
    yield_unit: Optional[str]
    cost: Optional[float]


class RecipeQueries:
    def get_recipes(self) -> List[RecipeOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, yield_amount, yield_unit, cost
                    FROM recipes
                    """,
                )
                result = db.fetchall()
                return [self.record_to_recipe_out(record) for record in result]

    def get_recipe(self, recipe_id: int) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, yield_amount, yield_unit, cost
                    FROM recipes
                    WHERE id = %s
                    """,
                    [recipe_id],
                )
                result = db.fetchone()
                return self.record_to_recipe_out(result)

    def create_recipe(self, recipe: RecipeIn) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO recipes (
                    name, yield_amount, yield_unit, cost
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, name, yield_amount, yield_unit, cost
                    """,
                    [
                        recipe.name,
                        recipe.yield_amount,
                        recipe.yield_unit,
                        recipe.cost,
                    ],
                )
                id = result.fetchone()[0]
                return self.recipe_in_to_out(id, recipe)

    def edit_recipe(self, id: int, recipe: RecipeIn) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE recipes
                    SET name = %s,
                        yield_amount = %s,
                        yield_unit = %s,
                        cost = %s
                    WHERE id = %s
                    """,
                    [
                        recipe.name,
                        recipe.yield_amount,
                        recipe.yield_unit,
                        recipe.cost,
                        id,
                    ],
                )
                return self.recipe_in_to_out(id, recipe)

    def record_to_recipe_out(self, record):
        return RecipeOut(
            id=record[0],
            name=record[1],
            yield_amount=record[2],
            yield_unit=record[3],
            cost=record[4],
        )

    def recipe_in_to_out(self, id: int, recipe: RecipeIn):
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)

    def delete_recipe(self, recipe_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM recipes
                    WHERE id = %s
                    """,
                    [recipe_id],
                )
                return True

    def get_recipe_cost(self, id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                        SELECT sum(i.cost_amount * iv.amount) as total
                        FROM ingredients as i
                        INNER JOIN ingredient_version as iv
                        ON i.name = iv.name
                        WHERE iv.recipe = %s
                        """,
                    [id],
                )
                result = db.fetchone()
                total = result[0] if result else None

        return total
