from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import Optional, List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class CocktailIn(BaseModel):
    name: str
    base_cost: Optional[float]
    secondary_cost: Optional[float]
    tertiary_cost: Optional[float]
    mixer_cost: Optional[float]
    price: Optional[float]
    drinks_per_keg: Optional[float]


class CocktailOut(CocktailIn):
    id: int


class CocktailMargin(BaseModel):
    id: int
    name: str
    total: float
    price: float
    margin: float
    suggested_retail: float


class CocktailQueries:
    def get_cocktails(self) -> List[CocktailOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, base_cost, secondary_cost, tertiary_cost, mixer_cost, price, drinks_per_keg
                    FROM cocktail
                    """,
                )
                result = db.fetchall()
                return [
                    self.record_to_cocktail_out(record) for record in result
                ]

    def get_margins(self) -> List[CocktailMargin]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT c.id,
                    c.name,
                    ((c.base_cost + c.secondary_cost + c.tertiary_cost + c.mixer_cost) * 1.02) / c.drinks_per_keg as total,
                    c.price,
                    100 * (((c.price * 0.90625)-(((c.base_cost + c.secondary_cost + c.tertiary_cost + c.mixer_cost) * 1.02) / c.drinks_per_keg)) / (c.price * 0.90625)) as margin,
                    1.09375 * (((c.base_cost + c.secondary_cost + c.tertiary_cost + c.mixer_cost) * 1.02) / c.drinks_per_keg) / .15 as suggested_retail_85

                    FROM cocktail as c
                    """,
                )
                result = db.fetchall()
                return [self.record_to_margin_out(record) for record in result]

    def get_cocktail(self, cocktail_id: int) -> CocktailOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, base_cost, secondary_cost, tertiary_cost, mixer_cost, price, drinks_per_keg
                    FROM cocktail
                    WHERE id = %s
                    """,
                    [cocktail_id],
                )
                result = db.fetchone()
                return self.record_to_cocktail_out(result)

    def create_cocktail(self, cocktail: CocktailIn) -> CocktailOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO cocktail (
                        name, base_cost, secondary_cost, tertiary_cost, mixer_cost, price, drinks_per_keg
                    )
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, name, base_cost, secondary_cost, tertiary_cost, mixer_cost, price, drinks_per_keg
                    """,
                    [
                        cocktail.name,
                        cocktail.base_cost,
                        cocktail.secondary_cost,
                        cocktail.tertiary_cost,
                        cocktail.mixer_cost,
                        cocktail.price,
                        cocktail.drinks_per_keg,
                    ],
                )
                id = result.fetchone()[0]
                return self.cocktail_in_to_out(id, cocktail)

    def edit_cocktail(self, id: int, cocktail: CocktailIn) -> CocktailOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE cocktail
                    SET name = %s,
                    base_cost = %s,
                    secondary_cost = %s,
                    tertiary_cost = %s,
                    mixer_cost = %s,
                    price = %s,
                    drinks_per_keg = %s
                    WHERE id = %s
                    """,
                    [
                        cocktail.name,
                        cocktail.base_cost,
                        cocktail.secondary_cost,
                        cocktail.tertiary_cost,
                        cocktail.mixer_cost,
                        cocktail.price,
                        cocktail.drinks_per_keg,
                        id,
                    ],
                )
                return self.cocktail_in_to_out(id, cocktail)

    def record_to_cocktail_out(self, record):
        return CocktailOut(
            id=record[0],
            name=record[1],
            base_cost=record[2],
            secondary_cost=record[3],
            tertiary_cost=record[4],
            mixer_cost=record[5],
            price=record[6],
            drinks_per_keg=record[7],
        )

    def record_to_margin_out(self, record):
        return CocktailMargin(
            id=record[0],
            name=record[1],
            total=record[2],
            price=record[3],
            margin=record[4],
            suggested_retail=record[5],
        )

    def cocktail_in_to_out(self, id: int, cocktail: CocktailIn):
        old_data = cocktail.dict()
        return CocktailOut(id=id, **old_data)

    def delete(self, cocktail_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execuute(
                    """
                    DELETE FROM cocktail
                    WHERE id = %s
                    """,
                    [cocktail_id],
                )
                return True
